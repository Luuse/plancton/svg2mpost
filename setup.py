
#! /usr/bin/env python2


from setuptools import setup


setup(
    name='svg2mpost',
    version='0.1',
    author='The active archives contributors',
    author_email='antoine@luuse.io',
    description='Convert svg file to metapost basic file',
    url='http://activearchives.org/',
    packages=['svg2mpost'],
    include_package_data=True,
    install_requires=[
        'lxml',
        'svg.path',
    ],
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Operating System :: OS Independent',
        'License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)',
        'Intended Audience :: Developers',
        'Environment :: Web Environment',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Indexing/Search',
    ]
)
